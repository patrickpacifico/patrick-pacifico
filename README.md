# Avaliação Front-End N1 #

Leia atentamente até o final.

Nossa avaliação é bem simples, pois o intuito é analisar como o candidato desenvolve os itens solicitados.
Avaliamos qualidade de código, versionamento, uso de automatizadores, uso de preprocessadores e etc.

Patrick Pacífico da Silva

*OBS.: Evite bootstrap e outros similares, pois queremos avaliar o seu código na implementação dos itens.*

### O Básico a ser executado para concorrer a vaga ###
* Crie uma branch com seu nome e desenvolva nela;
-- Fiz a criação da Branch em meu repositório e fiz uma Pull Request para a Branch original.

* Desenvolva o layout. O projeto deve ser responsivo. Lembrando que no layout só existe a versão desktop. Sendo assim adapte a responsividade como preferir.
-- Fiz o desenvolvimento do layout como foi pedido, sem a utilização de automatizadores.
-- Codifiquei em HTM5, CSS3 e JS. Tudo puro do zero.
-- Algumas funcionalidades JS tive o auxílio de pesquisas rápidas.
-- Modifiquei a cor dos SVG diretamente no código do CSS.
-- Utilizei o produto "Mario" como "index.html"
-- Criei o form do "FRETE" com seus name's e function pensando em um utilização futura, criando o JS para tal.

Finalizando esses itens você terá terminado o nível 01 da avaliação. E já estará apto para vaga de **JR**;

*OBS.: Interações e funcionalidades não sugeridas no layout serão levadas em consideração.*
-- Adicionei mais dois produtos "Dr. Strange" e "Ryu". (ambos com suas páginas)
-- Crie interação com os produtos listados na parte inferior.

### Ultrapassando a concorrência ###
* Observe que no layout existe um modal(Quick cart) de sucesso da compra. Ele deve aparecer quando clicar no botão de comprar.
-- Fiz a funcionalidade de sucesso de compra com modal, utilizando um pouco de JS e classes CSS para apontamento.

* **PLUS:** Junto com a ação de exibição do modal atualize a quantidade de itens no carrinho presente no header.
--- FUNCIONALIDADE NÃO EFETUADA -> Mas CONSIGO fazer com o auxílio de Ajax/JQuerry, não o fiz pelo fato de querer codificar tudo conforme foi pedido, sem otimizadores e sem utilizar bibliotecas.

Finalizando esses itens você terá terminado o nível 02 da avaliação. E já estará apto para vaga de **PLENO**;

### CHEFÃO para garantir a vaga: ###
* Desenvolver um autocomplete para busca no header.
--- FUNCIONALIDADE NÃO EFETUADA -> Mas CONSIGO fazer com o auxílio de JQuerry, não o fiz pelo fato de querer codificar tudo conforme foi pedido, sem otimizadores e sem utilizar bibliotecas.

*OBS. 01: Colocamos os principais arquivos a serem utilizados no repositório.*

*OBS. 02: O candidato esta livre para trabalhar com a estrutura e tecnologia que preferir, exceto bootstrap e similares.*

*OBS. 03: Qualquer dúvida você pode entrar em contato comigo pelo e-mail: rafael.augusto@agencian1.com.br ou skype: rafael.asb.*

*OBS. 04: Utilizar React conta bastante para a vaga de PLENO, se conseguir cumprir as etapas básicas.*
